# Obsah

- [Obsah](#obsah)
- [Semestrální práce - checklist požadavků](#semestrální-práce---checklist-požadavků)
  - [Seznam patternů](#seznam-patternů)
- [Návod pro spuštění a použití](#návod-pro-spuštění-a-použití)
  - [1) Naklonovat tento repozitář](#1-naklonovat-tento-repozitář)
  - [2) Spustit elasticsearch](#2-spustit-elasticsearch)
  - [3) Instalovat common-library](#3-instalovat-common-library)
  - [4) Spustit services](#4-spustit-services)
  - [4) Inicializace](#4-inicializace)
  - [5) Použití aplikace](#5-použití-aplikace)

# Semestrální práce - checklist požadavků

| Požadavek | Stav | Popis použití |
|----------|------|---------------|
| Výběr vhodné technologie a jazyka | ✅ | Použita technologie Java/SpringBoot |
| Readme v gitu s popisem co je hotové | ✅ | |
| Využití společné DB | ✅ | Použita H2 databáze, ORM Hibernate|
| Využití cache | ✅ | Pomocí Spring annotation - použití v MS1 Courses, Students - find by id, find all |
| Využití messaging principu | ❌ |  |
| Zabezpečení | ✅ | Basic auth nad teacher metodami v Account service |
| Využití Interceptorů | ✅ | Class service logging interceptory |
| Využití REST | ✅ | API komunikace přes REST |
| Nasazení na produkční server například Heroku | ❌ | - |
| Výběr vhodné architektury | ✅ | Použita architektura microservices |
| Inicializační postup | ✅ | Popsáno v dokumentaci |
| Využití Elasticsearch | ✅ | Implementováno pro vyhledávání Courses pomocí více parametrů (name, capacity, credits, teacher id) |
| Použití alespoň 5 design patternů | ✅ | Builder, Facade, Factory, DTO / Adapter, Singleton |
| Za každého člena týmu 2 UC | ✅ | CRUD schedule & location & class & course & student & teacher, show schedule for user id , show detailed classes for schedule id, join course as student, join class as student, get teachers location detail |
| Cloud služby | ❌ |  |

## Seznam patternů

- Builder - TeacherAccountBuilder, StudentAccountBuilder v ms1
- Facade - ClassRemovalFacade v ms2
- Factory - SClassFactory v ms2
- DTO / Adapter - dto složka v ms1
- Singleton - vše s anotací Component, Service, Repository, Controller

# Návod pro spuštění a použití
## 1) Naklonovat tento repozitář
```
https://gitlab.fel.cvut.cz/jelinjon/nss_project
```
 ## 2) Spustit elasticsearch
V adresáři nss_project/elasticsearch-8.14.1/bin spustit soubor elasticsearch.bat (pro windows), například vykonáním příkazu `.\elasticsearch.bat`.

Pro spuštění Elasticsearch je třeba mít dostatek volné ram paměti.

Následně se spustí služba elasticsearch na default portu 9200. Elasticsearch musí běžet před zapnutím ms1!

## 3) Instalovat common-library
Po navigaci do složky nss_project/common_library je třeba vykonat `mvn clean install` aby fungovaly správně dependencies v ms1 a ms2.

## 4) Spustit services
Services ms1 a ms2 se pustí přes funkci main ve třídách KosApplication.java
Adresy aplkací jsou následující:

Account service / ms1 - `http:localhost:8081/AccountService`

Class service / ms2 - `http:localhost:8082/ClassService`

Gateway api - `http:localhost:8080/`

Pro monitoring metrik lze spustit i Prometheus, ve složce nss_project/prometheus-2.53.0.windows-amd64 spustit soubor prometheus.exe
Prometheus data pak budou dostupná na adrese:<br>
`http:localhost:8081/AccountService/actuator/prometheus` <br>
nebo<br>
`http:localhost:8080/actuator/prometheus`

## 4) Inicializace
Po spuštění všech služeb a Elasticsearch se při spuštění programu automaticky inicializuje databáze s položkami definovanými v data.sql scriptu a v SystemInit třídách obouch servis.


## 5) Použití aplikace
Gateway api běží na localhost:8080, routuje requesty na microservisy na portech 8081 a 8082.

Některé cesty požadavků jsou zamknuté za Basic Auth, pro přístup k nim je třeba přidat v Postman basic auth a vložit username a password které existuje v databázi. Ošetřené metody jsou ty, které mají adresu .../teacher**

**Doporučuji** použít učitelská account vygenerovaný po načtení systému s uživatelskými daty
`username: admin; password: heslo`

Při vykonávání určitých funkcí spolu microservisy  vzájěmně komunikují, je třeba je mít obě spuštěné.