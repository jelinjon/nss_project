package ear.kos2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/test/**")
                        .filters(f -> f
                                .prefixPath("/AccountService")
                        )
                        .uri("http://localhost:8081")
                ) // test route
                .route(r -> r.path("/account/**")
                        .filters(f -> f
                                .prefixPath("/AccountService")
                        )
                        .uri("http://localhost:8081")
                )
                .route(r -> r.path("/student/**")
                        .filters(f -> f
                                .prefixPath("/AccountService")
                        )
                        .uri("http://localhost:8081")
                )
                .route(r -> r.path("/teacher/**")
                        .filters(f -> f
                                .prefixPath("/AccountService")
                        )
                        .uri("http://localhost:8081")
                )
                .route(r -> r.path("/courses/**")
                        .filters(f -> f
                                .prefixPath("/AccountService")
                        )
                        .uri("http://localhost:8081")
                )
                .route(r -> r.path("/schedule/**")
                        .filters(f -> f
                                .prefixPath("/ClassService")
                        )
                        .uri("http://localhost:8082")
                )
                .route(r -> r.path("/class/**")
                        .filters(f -> f
                                .prefixPath("/ClassService")
                        )
                        .uri("http://localhost:8082")
                )
                .route(r -> r.path("/location/**")
                        .filters(f -> f
                                .prefixPath("/ClassService")
                        )
                        .uri("http://localhost:8082")
                )
                .route(r -> r.path("/actuator/**")
                        .filters(f -> f
                                .prefixPath("/AccountService")
                        )
                        .uri("http://localhost:8081")
                )
                .build();
    }

}
