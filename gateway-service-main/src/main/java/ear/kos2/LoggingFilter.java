package ear.kos2;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class LoggingFilter extends AbstractGatewayFilterFactory<LoggingFilter.Config> {

    public LoggingFilter() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            logRequest(exchange);
            return chain.filter(exchange).then(Mono.fromRunnable(() -> {
                logResponse(exchange);
            }));
        };
    }

    private void logRequest(ServerWebExchange exchange) {
        System.out.println("Original Request URI: " + exchange.getRequest().getURI());
    }

    private void logResponse(ServerWebExchange exchange) {
        System.out.println("Response Status Code: " + exchange.getResponse().getStatusCode());
    }

    public static class Config {
    }
}
