INSERT INTO location (street, building, room) VALUES ('Nad Bukem', 'building A', '317');

INSERT INTO class (number, type, capacity, course)
VALUES ('101a', 'LECTURE', 100, 1);

INSERT INTO class (number, type, capacity, course)
VALUES ('102a', 'LECTURE', 200, 2);

INSERT INTO class (number, type, capacity, course)
VALUES ('103a', 'LECTURE', 220, 3);

INSERT INTO class (number, type, capacity, course)
VALUES ('101b', 'EXERCISE', 20, 1);

INSERT INTO class (number, type, capacity, course)
VALUES ('102b', 'EXERCISE', 16, 2);

INSERT INTO class (number, type, capacity, course)
VALUES ('103b', 'EXERCISE', 30, 3);

INSERT INTO schedule (owner, semester)
VALUES (1, 'summer');
INSERT INTO schedule (owner, semester)
VALUES (2, 'summer');
INSERT INTO schedule (owner, semester)
VALUES (3, 'winter');

-- Insert class IDs into the schedule_classes table for schedule ID 1
INSERT INTO schedule_classes (schedule_id, class_id) VALUES (1, 1);
INSERT INTO schedule_classes (schedule_id, class_id) VALUES (1, 2);

-- Insert class IDs into the schedule_classes table for schedule ID 2
INSERT INTO schedule_classes (schedule_id, class_id) VALUES (2, 3);
INSERT INTO schedule_classes (schedule_id, class_id) VALUES (2, 4);

-- Insert class IDs into the schedule_classes table for schedule ID 3
INSERT INTO schedule_classes (schedule_id, class_id) VALUES (3, 5);
INSERT INTO schedule_classes (schedule_id, class_id) VALUES (3, 6);