package ear.kos2.model;

import jakarta.persistence.*;
import java.util.List;

@Entity
@Table(name = "class")
public class SClass extends AbstractEntity {
    @Basic
    @Column(name = "number", nullable = true, length = 20)
    private String number;
    @Basic
    @Column(name = "type", nullable = true)
    private String type;
    @Basic
    @Column(name = "capacity", nullable = true)
    private Integer capacity;
    @Basic
    @Column(name = "course", nullable = true)
    private Integer course;
    @Basic
    @Column(name = "teachers", nullable = true)
    private List<Integer> teachers;
    @ManyToOne
    @JoinColumn(name = "location")
    private Location location;


    public SClass(){
    }

    public void addTeacher(Integer teacherId) {
        this.teachers.add(teacherId);
    }

    @Override
    public String toString(){
        return "Class{ " + "number=" + number + ", type=" + type + ", capacity=" + capacity
                + ", course=" + course + ", location=" + location + "}";
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public List<Integer> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Integer> teachers) {
        this.teachers = teachers;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
