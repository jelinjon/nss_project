package ear.kos2.model;

import java.util.List;

public class SClassFactory {

    private SClassFactory() {
    }

    public static SClass createSClass() {
        return new SClass();
    }

    public static SClass createSClass(String number, String type, Integer capacity, Integer course, List<Integer> teachers, Location location) {
        SClass sClass = new SClass();
        sClass.setNumber(number);
        sClass.setType(type);
        sClass.setCapacity(capacity);
        sClass.setCourse(course);
        sClass.setTeachers(teachers);
        sClass.setLocation(location);
        return sClass;
    }

    public static SClass createDefaultLecture() {
        SClass sClass = new SClass();
        sClass.setNumber("101");
        sClass.setType("LECTURE");
        sClass.setCapacity(200);
        sClass.setCourse(1);
        sClass.setTeachers(List.of());
        return sClass;
    }

    public static SClass createDefaultExcercise() {
        SClass sClass = new SClass();
        sClass.setNumber("102");
        sClass.setType("EXERCISE");
        sClass.setCapacity(20);
        sClass.setCourse(1);
        sClass.setTeachers(List.of());
        return sClass;
    }
}
