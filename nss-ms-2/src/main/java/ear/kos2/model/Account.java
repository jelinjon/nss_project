package ear.kos2.model;

import jakarta.persistence.*;
import org.springframework.security.crypto.password.PasswordEncoder;

@Entity
@Table(name = "account", uniqueConstraints = {@UniqueConstraint(columnNames = {"username"})})
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
        @NamedQuery(name = "Account.findByUsername", query = "SELECT u FROM Account u WHERE u.username = :username")
})
public abstract class Account extends AbstractEntity {
    @Basic
    @Column(name = "username", nullable = false, length = 20)
    protected String username;
    @Basic
    @Column(name = "password", nullable = false)
    protected String password;
    @Basic
    @Column(name = "first_name", nullable = false, length = 20)
    protected String firstName;
    @Basic
    @Column(name = "last_name", nullable = false, length = 20)
    protected String lastName;
//    @OneToOne(mappedBy = "owner")
//    @JoinTable(name = "schedule", joinColumns = @JoinColumn(name = "id"))
//    protected Schedule schedule;

    @Basic
    @Column(name = "scheduleId", nullable = true, length = 20)
    protected Integer scheduleId;
    protected Boolean online;

    protected Role role;

    public Integer getSchedule() {
        return this.scheduleId;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public Boolean getOnline() {
        return online;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole(){
        if(role == null){
            return Role.STUDENT;
        }
        return role;
    }

    public void setRole(Role role){
        this.role = role;
    }

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }

    @Override
    public String toString(){
        return "Account{ " + "username=" + username + ", first name=" + firstName + ", last name=" + lastName + "}";
    }
}
