package ear.kos2.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "schedule")
@NamedQueries({
        @NamedQuery(name = "Schedule.findByUserId", query = "SELECT u FROM Schedule u WHERE u.owner = :userId")
})
public class Schedule extends AbstractEntity {
    @Basic
    @Column(name = "owner", nullable = false, length = 20)
    private Integer owner;
    @Basic
    @Column(name = "semester", nullable = false, length = 20)
    private String semester;

    @ElementCollection
    @CollectionTable(name = "schedule_classes", joinColumns = @JoinColumn(name = "schedule_id"))
    @Column(name = "class_id")
    private List<Integer> classIds;


    public Schedule() {}

    public Schedule(Integer owner) {
        this.owner = owner;
    }

    public void addClassId(Integer classId){
        if(! this.classIds.contains(classId)) {
            this.classIds.add(classId);
        }
    }
    public boolean removeClassId(Integer classId){
        return this.classIds.remove(classId);
    }

    @Override
    public String toString(){
        return "Schedule{ " + "owner=" + owner + ", semester=" + semester + ", classes=" + classIds.toString() +"}";

    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public List<Integer> getClassIds() {
        return classIds;
    }

    public void setClassIds(List<Integer> classIds) {
        this.classIds = classIds;
    }
}
