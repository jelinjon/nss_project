package ear.kos2.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@Component
public class LoggerInterceptor implements HandlerInterceptor {
    private java.util.logging.Logger LOG = Logger.getLogger("UserService");

    public LoggerInterceptor() {
        System.out.println(System.getProperty("user.dir"));
        try {
            FileHandler handler = new FileHandler(System.getProperty("user.dir") + "\\UserService.log");
            LOG.addHandler(handler);
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LOG.info("Pre-handle " + request + ": " + request.getMethod() + "\nURI: " + request.getRequestURI());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        LOG.info("Post-handle " + request + ": " + request.getMethod() + "\nURI: " + request.getRequestURI());
        System.out.println("Post Handle was Called");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if(ex!=null){
            LOG.info("Encountered error: " + ex.getMessage());
        }
        LOG.info("Request " + request + " completed");
    }

}