package ear.kos2.proxy;

import ear.kos2.model.Schedule;
import ear.kos2.service.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class ClassRemovalProxy {
    private static final Logger LOG = LoggerFactory.getLogger(ClassRemovalProxy.class);
    private final ScheduleService scheduleService;
    @Autowired
    public ClassRemovalProxy(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    public void removeClassFromSchedules(Integer classId) {
        if (classId == null) {
            throw new IllegalArgumentException("Class ID cannot be null");
        }

        List<Schedule> allSchedules = scheduleService.findAll();
        if (allSchedules == null || allSchedules.isEmpty()) {
            LOG.warn("No schedules found");
            return;
        }

        for (Schedule schedule : allSchedules) {
            if(schedule.removeClassId(classId)){
                scheduleService.update(schedule);
                LOG.info("Removed class ID {} from schedule ID {}", classId, schedule.getId());
            }
        }
    }
}
