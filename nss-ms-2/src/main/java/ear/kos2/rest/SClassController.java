package ear.kos2.rest;

import ear.kos2.exception.NotFoundException;
import ear.kos2.exception.ValidationException;
import ear.kos2.proxy.ClassRemovalProxy;
import ear.kos2.model.SClass;
import ear.kos2.service.SClassService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/class")
public class SClassController {
    private static final Logger LOG = LoggerFactory.getLogger(SClassController.class);

    private final SClassService sclassService;
    private final ClassRemovalProxy classRemovalProxy;

    @Autowired
    public SClassController(SClassService sclassService, ClassRemovalProxy classRemovalProxy) {
        this.sclassService = sclassService;
        this.classRemovalProxy = classRemovalProxy;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SClass> getClasses() {
        LOG.trace("Entering getClasses method.");
        List<SClass> classes = sclassService.findAll();
        LOG.info("Found {} classes.", classes.size());
        return classes;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SClass> createSClass(@RequestBody SClass sclass) {
        LOG.trace("Entering createSClass method with payload: {}", sclass);
        try {
            sclassService.persist(sclass);
            LOG.debug("Created sclass {}.", sclass);
            return ResponseEntity.status(HttpStatus.CREATED).body(sclass);
        }
        catch (ValidationException e) {
            LOG.warn("Validation failed for sclass {}: {}", sclass, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        catch (Exception e) {
            LOG.error("Error creating sclass {}: {}", sclass, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SClass getSClass(@PathVariable Integer id) {
        LOG.trace("Entering getSClass method with ID: {}", id);
        final SClass p = sclassService.find(id);
        if (p == null) {
            LOG.warn("SClass with ID {} not found.", id);
            throw NotFoundException.create("SClass", id);
        }
        LOG.info("Found sclass with ID {}: {}", id, p);
        return p;
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSClass(@PathVariable Integer id, @RequestBody SClass sclass) {
        LOG.trace("Entering updateSClass method with ID: {} and payload: {}", id, sclass);
        final SClass original = getSClass(id);
        if (!original.getId().equals(sclass.getId())) {
            LOG.warn("SClass ID mismatch: URL ID {} does not match payload ID {}.", id, sclass.getId());
            throw new ValidationException("SClass identifier in the data does not match the one in the request URL.");
        }
        sclassService.update(sclass);
        LOG.debug("Updated sclass {}.", sclass);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeSClass(@PathVariable Integer id) {
        LOG.trace("Entering removeSClass method with ID: {}", id);
        final SClass toRemove = sclassService.find(id);
        if (toRemove == null) {
            LOG.warn("SClass with ID {} not found for deletion.", id);
            return;
        }
        classRemovalProxy.removeClassFromSchedules(id);
        sclassService.remove(toRemove);
        LOG.debug("Removed sclass {}.", toRemove);
    }
}

