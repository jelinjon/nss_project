package ear.kos2.rest;

import ear.kos2.exception.NotFoundException;
import ear.kos2.exception.ValidationException;
import ear.kos2.model.Location;
import ear.kos2.model.LocationDTO;
import ear.kos2.util.RestUtils;
import ear.kos2.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/location")
public class LocationController {
    private static final Logger LOG = LoggerFactory.getLogger(LocationController.class);
    private final LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Location> getLocations() {
        LOG.trace("Entering getLocations method.");

        LOG.debug("Fetching all locations.");
        List<Location> locations = locationService.findAll();
        LOG.debug("Fetched {} locations.", locations.size());

        LOG.info("Fetched {} locations.", locations.size());
        LOG.trace("Exiting getLocations method.");
        return locations;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createLocation(@RequestBody Location location) {
        LOG.trace("Entering createLocation method.");

        try {
            locationService.persist(location);
            LOG.info("Created location {}.", location);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", location.getId());

            LOG.trace("Exiting createLocation method.");
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        catch (Exception e) {
            LOG.error("Error creating location: {}", location, e);
            throw e;
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LocationDTO getLocation(@PathVariable Integer id) {
        LOG.trace("Entering getLocation method.");

        LOG.debug("Fetching location with id: {}.", id);
        try {
            final Location location = locationService.find(id);
            if (location == null) {
                LOG.warn("Location with id {} not found.", id);
                throw NotFoundException.create("Location", id);
            }
            LOG.info("Fetched location: {}.", location);

            LOG.trace("Exiting getLocation method.");
            return convertToLocationDTO(location);
        }
        catch (Exception e) {
            LOG.error("Error fetching location with id: {}", id, e);
            throw e;
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateLocation(@PathVariable Integer id, @RequestBody Location location) {
        LOG.trace("Entering updateLocation method.");

        LOG.debug("Updating location with id: {}.", id);
        try {
            final Location original = locationService.find(id);
            if (!original.getId().equals(id)) {
                LOG.warn("Validation failed for location update with id {}: identifier mismatch.", id);
                throw new ValidationException("Location identifier in the data does not match the one in the request URL.");
            }
            original.setBuilding(location.getBuilding());
            original.setRoom(location.getRoom());
            original.setStreet(location.getStreet());
            locationService.update(original);
            LOG.info("Updated location {}.", location);

            LOG.trace("Exiting updateLocation method.");
        }
        catch (Exception e) {
            LOG.error("Error updating location with id: {}", id, e);
            throw e;
        }
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeLocation(@PathVariable Integer id) {
        LOG.trace("Entering removeLocation method.");

        LOG.debug("Removing location with id: {}.", id);
        try {
            final Location toRemove = locationService.find(id);
            if (toRemove == null) {
                LOG.warn("Location with id {} not found for removal.", id);
                return;
            }
            locationService.remove(toRemove);
            LOG.info("Removed location {}.", toRemove);

            LOG.trace("Exiting removeLocation method.");
        }
        catch (Exception e) {
            LOG.error("Error removing location with id: {}", id, e);
            throw e;
        }
    }

    public LocationDTO convertToLocationDTO(Location location){
        LocationDTO temp = new LocationDTO();
        temp.setId(location.getId());
        temp.setBuilding(location.getBuilding());
        temp.setRoom(location.getRoom());
        temp.setStreet(location.getStreet());
        return temp;
    }
}

