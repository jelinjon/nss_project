package ear.kos2.rest;

import ear.kos2.exception.ValidationException;
import ear.kos2.model.SClass;
import ear.kos2.model.Schedule;
import ear.kos2.model.ScheduleDTO;
import ear.kos2.util.RestUtils;
import ear.kos2.service.SClassService;
import ear.kos2.service.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/schedule")
public class ScheduleController {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleController.class);
    private final ScheduleService scheduleService;
    private final SClassService sclassService;

    @Autowired
    public ScheduleController(
            ScheduleService scheduleService,
            SClassService sclassService
    ) {
        this.scheduleService = scheduleService;
        this.sclassService = sclassService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Schedule> getSchedules() {
        LOG.trace("Fetching all schedules.");
        return scheduleService.findAll();
    }

    @GetMapping(value = "/{id}/classes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SClass> getClassesForScheduleId(@PathVariable Integer id) {
        LOG.trace("Fetching classes for schedule ID: {}", id);
        return scheduleService.findClassesByScheduleId(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path = "/create")
    public ResponseEntity<Void> createSchedule(@RequestBody Schedule schedule) {
        scheduleService.persist(schedule);
        LOG.debug("Created schedule {}.", schedule);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", schedule.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Schedule getSchedule(@PathVariable Integer id) {
        LOG.trace("Fetching schedule with ID: {}", id);
        final Schedule p = scheduleService.find(id);
        if (p == null) {
            LOG.info("Schedule with id " + id + " was not found.");
            return null;
        }
        return p;
    }
    @GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ScheduleDTO getScheduleForUserId(@PathVariable Integer id) {
        LOG.trace("Fetching schedule for user ID: {}", id);
        final Schedule schedule = scheduleService.findByUserId(id);
        if (schedule == null) {
            LOG.info("Schedule with owner with id " + id + " was not found.");
            return null;
        }
        return convertToScheduleDTO(schedule);
    }
    @PostMapping(value = "/add_class/{classId}/student/{studentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addClassToSchedule(@PathVariable Integer classId, @PathVariable Integer studentId) {
        LOG.info("Adding class with ID: {} to schedule for student ID: {}", classId, studentId);
        try {
            final Schedule tempSchedule = scheduleService.findByUserId(studentId);
            if (tempSchedule == null) {
                LOG.warn("Schedule not found for student ID: {}", studentId);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Schedule not found for student ID: " + studentId);
            }

            final SClass tempClass = sclassService.find(classId);
            if (tempClass == null) {
                LOG.warn("Class not found for class ID: {}", classId);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Class not found for class ID: " + classId);
            }
            tempSchedule.addClassId(classId);
            scheduleService.update(tempSchedule);
            LOG.debug("Added class ID {} to schedule for student ID {}.", classId, studentId);
            return ResponseEntity.status(HttpStatus.OK).body("Added class ID " + classId + " to schedule for student ID " + studentId);
        } catch (Exception e) {
            LOG.error("An error occurred while adding class to schedule for student ID: {}", studentId, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred while adding class to schedule");
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSchedule(@PathVariable Integer id, @RequestBody Schedule schedule) {
        LOG.trace("Updating schedule with ID: {}", id);
        final Schedule original = getSchedule(id);
        if (!original.getId().equals(schedule.getId())) {
            LOG.warn("Schedule ID mismatch: {} != {}", original.getId(), schedule.getId());
            throw new ValidationException("Schedule identifier in the data does not match the one in the request URL.");
        }
        scheduleService.update(schedule);
        LOG.debug("Updated schedule {}.", schedule);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeSchedule(@PathVariable Integer id) {
        LOG.trace("Removing schedule with ID: {}", id);
        final Schedule toRemove = scheduleService.find(id);
        if (toRemove == null) {
            LOG.warn("Schedule to remove with ID {} not found.", id);
            return;
        }
        scheduleService.remove(toRemove);
        LOG.debug("Removed schedule {}.", toRemove);
    }

    public ScheduleDTO convertToScheduleDTO(Schedule schedule) {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setId(schedule.getId());
        scheduleDTO.setOwner(schedule.getOwner());
        scheduleDTO.setSemester(schedule.getSemester());
        scheduleDTO.setClassIds(schedule.getClassIds());
        return scheduleDTO;
    }
}

