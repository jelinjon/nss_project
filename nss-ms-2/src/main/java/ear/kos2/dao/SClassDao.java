package ear.kos2.dao;

import ear.kos2.model.SClass;
import org.springframework.stereotype.Repository;

@Repository
public class SClassDao extends BaseDao<SClass>{

    protected SClassDao() {
        super(SClass.class);
    }
}
