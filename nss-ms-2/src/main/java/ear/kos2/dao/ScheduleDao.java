package ear.kos2.dao;

import ear.kos2.model.SClass;
import ear.kos2.model.Schedule;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ScheduleDao extends BaseDao<Schedule>{
    protected ScheduleDao() {
        super(Schedule.class);
    }

    public Schedule findByUserId(Integer id) {
        try {
            return em.createNamedQuery("Schedule.findByUserId", Schedule.class).setParameter("userId", id)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<SClass> findClassesByScheduleId(Integer scheduleId) {
        TypedQuery<SClass> query = em.createQuery(
                "SELECT sc FROM SClass sc WHERE sc.id IN (SELECT scId FROM Schedule s JOIN s.classIds scId WHERE s.id = :scheduleId)",
                SClass.class
        );
        query.setParameter("scheduleId", scheduleId);
        return query.getResultList();
    }
}
