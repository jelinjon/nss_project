package ear.kos2.service;

import ear.kos2.dao.ScheduleDao;
import ear.kos2.model.SClass;
import ear.kos2.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class ScheduleService implements GenericService<Schedule>{

    private final ScheduleDao dao;

    @Autowired
    public ScheduleService(ScheduleDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Schedule> findAll() {
        return dao.findAll();
    }

    @Override
    public Schedule find(Integer id) {
        return dao.find(id);
    }

    @Transactional(readOnly = true)
    public Schedule findByUserId(Integer id) {
        return dao.findByUserId(id);
    }

    @Transactional(readOnly = true)
    public List<SClass> findClassesByScheduleId(Integer scheduleId) {
        return dao.findClassesByScheduleId(scheduleId);
    }

    @Override
    public void persist(Schedule course) {
        dao.persist(course);
    }

    @Override
    public void persist(Collection<Schedule> entities) {
        dao.persist(entities);
    }

    @Override
    public Schedule update(Schedule course) {
        dao.update(course);
        return course;
    }

    @Override
    public void remove(Schedule course){
        dao.remove(course);
    }

    @Override
    public boolean exists(Integer id) {
        return dao.exists(id);
    }
}
