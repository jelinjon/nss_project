package ear.kos2.service;

import ear.kos2.dao.SClassDao;
import ear.kos2.model.SClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class SClassService implements GenericService<SClass>{

    private final SClassDao dao;

    @Autowired
    public SClassService(SClassDao dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    public List<SClass> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public SClass find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public void persist(SClass sclass) {
        dao.persist(sclass);
    }

    @Override
    public void persist(Collection<SClass> entities) {
        dao.persist(entities);
    }

    @Transactional
    public SClass update(SClass sclass) {
        dao.update(sclass);
        return sclass;
    }

    @Transactional
    public void remove(SClass sclass){
        dao.remove(sclass);
    }

    @Override
    public boolean exists(Integer id) {
        return dao.exists(id);
    }
}
