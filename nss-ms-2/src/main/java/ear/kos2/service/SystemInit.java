package ear.kos2.service;

import ear.kos2.model.Role;
import ear.kos2.model.SClass;
import ear.kos2.model.SClassFactory;
import ear.kos2.model.Schedule;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class SystemInit {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInit.class);

    private final PlatformTransactionManager txManager;
    private final ScheduleService scheduleService;
    private final SClassService sClassService;

    @Autowired
    public SystemInit(PlatformTransactionManager txManager, ScheduleService scheduleService, SClassService sClassService) {
        this.txManager = txManager;
        this.scheduleService = scheduleService;
        this.sClassService = sClassService;
    }

    @PostConstruct
    private void initSystem() {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute((status) -> {
            SClass temp1 = SClassFactory.createDefaultLecture();
            sClassService.persist(temp1);

            SClass temp2 = SClassFactory.createDefaultExcercise();
            sClassService.persist(temp2);
            return null;
        });
    }
}

