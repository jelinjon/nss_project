package ear.kos2.service.security;

import ear.kos2.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final RestTemplate restTemplate;

    @Autowired
    public UserDetailsService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account user = null;
        List tempUserList = List.of();

        String url = "http://localhost:8082/ClassService/account/" + username;
        try {
            tempUserList = restTemplate.getForObject(url, List.class);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        user = (Account) tempUserList.get(0);

        if (user == null) {
            throw new UsernameNotFoundException("Account with username " + username + " not found.");
        }
        return new ear.kos2.security.model.UserDetails(user);
    }
}

