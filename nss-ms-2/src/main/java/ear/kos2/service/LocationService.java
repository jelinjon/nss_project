package ear.kos2.service;

import ear.kos2.dao.LocationDao;
import ear.kos2.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class LocationService implements GenericService<Location> {
    private final LocationDao dao;

    @Autowired
    public LocationService(LocationDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Location> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public List<Location> findAll(String street) {
        return dao.findAllbyStreet(street);
    }

    @Override
    public Location find(Integer id) {
        return dao.find(id);
    }

    @Override
    public void persist(Location location) {
        dao.persist(location);
    }

    @Override
    public void persist(Collection<Location> entities) {
        dao.persist(entities);
    }

    @Override
    public Location update(Location location) {
        dao.update(location);
        return location;
    }

    @Override
    public void remove(Location location){
        dao.remove(location);
    }

    @Override
    public boolean exists(Integer id) {
        return dao.exists(id);
    }
}
