package ear.kos2.service;

import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;
import java.util.List;

public interface GenericService<T> {

    @Transactional(readOnly = true)
    T find(Integer id);

    @Transactional(readOnly = true)
    List<T> findAll();

    @Transactional
    void persist(T entity);

    @Transactional
    void persist(Collection<T> entities);

    @Transactional
    T update(T entity);

    @Transactional
    void remove(T entity);

    @Transactional
    boolean exists(Integer id);


}
