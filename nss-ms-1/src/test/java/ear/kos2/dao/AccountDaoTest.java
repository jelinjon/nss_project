package ear.kos2.dao;

import ear.kos2.KosApplication;
import ear.kos2.dao.AccountDao;
import ear.kos2.model.StudentAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ComponentScan(basePackageClasses = KosApplication.class)
@ActiveProfiles("test")
public class AccountDaoTest {
    @Autowired
    private TestEntityManager em;

    @Autowired
    private AccountDao sut;

    @Test
    public void accountExistsReturnsTrue(){
        StudentAccount studentTemp = new StudentAccount();
        studentTemp.setId(1);
        studentTemp.setUsername("user1");
        studentTemp.setPassword("password");

        em.persist(studentTemp);

        Assertions.assertTrue(sut.accountExists("user1", "password"));
    }

    @Test
    public void accountExistsReturnsFalse(){
        StudentAccount studentTemp = new StudentAccount();
        studentTemp.setId(1);
        studentTemp.setUsername("user1");
        studentTemp.setPassword("password");

        em.persist(studentTemp);

        Assertions.assertTrue(sut.accountExists("user2", "password"));
    }
}
