package ear.kos2.service;

import ear.kos2.KosApplication;
import ear.kos2.dao.CourseDao;
import ear.kos2.exception.AccountException;
import ear.kos2.model.Course;
import ear.kos2.service.TeacherAccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ComponentScan(basePackageClasses = KosApplication.class)
@ActiveProfiles("test")
public class TeacherAccountServiceTest {
    @Autowired
    private TestEntityManager em;
    @Autowired
    private CourseDao courseDao;

    @Autowired
    private TeacherAccountService sut;


    @Test
    public void editCourseEditsCourse(){
        Course course = new Course();
        course.setName("testCourse");
        course.setId(1);

        courseDao.persist(course);
        sut.editCourse(1, null, 5, 30);

        Course temp = courseDao.find(1);
        Assertions.assertEquals(10, temp.getCapacity());
        Assertions.assertEquals(5, temp.getCredits());
    }

    @Test
    public void editCourseThrowsException(){
        Course course = new Course();
        course.setName("testCourse");
        course.setId(1);

        courseDao.persist(course);
        Assertions.assertThrows(AccountException.class, ()->{sut.editCourse(2, null, 5, 30);});
    }

}
