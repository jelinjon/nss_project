package ear.kos2.service;

import ear.kos2.KosApplication;
import ear.kos2.dao.CourseDao;
import ear.kos2.exception.AccountException;
import ear.kos2.model.Course;
import ear.kos2.service.StudentAccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ComponentScan(basePackageClasses = KosApplication.class)
@ActiveProfiles("test")
public class StudentAccountServiceTest {
    @Autowired
    private TestEntityManager em;
    @Autowired
    private CourseDao courseDao;

    @Autowired
    private StudentAccountService sut;

    @Test
    public void addCourseAddsCourse(){
        Course course = new Course();
        course.setName("testCourse");
        course.setId(10);
        em.persist(course);

        sut.addCourse(10);

        Assertions.assertEquals(1, sut.showMyCourses().size());
    }
    @Test
    public void addCourseThrowsException(){
        Course course = new Course();
        course.setName("testCourse");
        course.setId(10);
        em.persist(course);

        Assertions.assertThrows(AccountException.class, ()->{ sut.addCourse(11);});
    }

}
