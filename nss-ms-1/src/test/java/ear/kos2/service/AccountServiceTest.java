package ear.kos2.service;

import ear.kos2.KosApplication;
import ear.kos2.model.Course;
import ear.kos2.service.AccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

@DataJpaTest
@ComponentScan(basePackageClasses = KosApplication.class)
@ActiveProfiles("test")
public class AccountServiceTest {

    @Autowired
    private TestEntityManager em;

    @Autowired
    private AccountService sut;

    @Test
    public void searchCoursesThrowsException(){
        Course course1 = new Course();
        course1.setName("course1");
        Course course2 = new Course();
        course2.setName("course1");

        em.persist(course1);
        em.persist(course2);

        Assertions.assertThrows(IllegalArgumentException.class, ()->{sut.searchCourses("course");});
    }

    @Test
    public void searchCoursesReturnsList(){
        Course course1 = new Course();
        course1.setName("course1");
        Course course2 = new Course();
        course2.setName("course1");

        em.persist(course1);
        em.persist(course2);

        List<Course> result;
        result = sut.searchCourses("course1");

        Assertions.assertEquals(2, result.size());
    }

}
