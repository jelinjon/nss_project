package ear.kos2.service;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

@Component
public class SystemInit {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInit.class);

    /**
     * Default admin username
     */
    private static final String TEACHER_USERNAME = "admin";
    private static final String TEACHER_PASSWORD = "heslo";
    private final AccountInit accountInit;

    private final PlatformTransactionManager txManager;

    @Autowired
    public SystemInit(AccountInit accountInit,
                             PlatformTransactionManager txManager) {
        this.txManager = txManager;
        this.accountInit = accountInit;
    }

    @PostConstruct
    private void initSystem() {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute((status) -> {
            generateData();
            return null;
        });
    }

    /**
     * Generates an teacher account if it does not already exist.
     */
    private void generateData() {
        accountInit.generateTeacherAccount(TEACHER_USERNAME, TEACHER_PASSWORD, "Antonín", "Kmín", 1);
        accountInit.generateStudentAccount("peter", "password1", "Petr", "Smidt");
        LOG.info("Generated teacher user with credentials " + TEACHER_USERNAME + "/" + TEACHER_PASSWORD);
        accountInit.generateCourse();

    }
}

