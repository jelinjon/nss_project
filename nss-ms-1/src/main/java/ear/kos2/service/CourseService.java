package ear.kos2.service;

import ear.kos2.dao.CourseDao;
import ear.kos2.model.Course;
import ear.kos2.model.StudentAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class CourseService implements GenericService<Course>{

    private final CourseDao dao;

    @Autowired
    public CourseService(CourseDao dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    @Cacheable(value = "courses", key = "#root.methodName")
    public List<Course> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public List<Course> findAll(String name) {
        return dao.findAll(name);
    }

    @Transactional(readOnly = true)
    @Cacheable(value = "courses", key = "#id")
    public Course find(Integer id) {
        return dao.find(id);
    }
    @Transactional(readOnly = true)
    @Cacheable(value = "courses", key = "#name")
    public List<Course> findByName(String name) {
        return dao.findAll(name);
    }

    @Transactional
    public void persist(Course course) {
        dao.persist(course);
    }

    @Override
    public void persist(Collection<Course> entities) {
        dao.persist(entities);
    }

    @Transactional
    public Course update(Course course) {
        dao.update(course);
        return course;
    }

    @Override
    public void remove(Course course){
        dao.remove(course);
    }

    @Override
    public boolean exists(Integer id) {
        return false;
    }
}
