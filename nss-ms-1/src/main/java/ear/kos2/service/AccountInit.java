package ear.kos2.service;

import ear.kos2.model.*;
import ear.kos2.service.CourseService;
import ear.kos2.service.StudentAccountService;
import ear.kos2.service.TeacherAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class AccountInit {
    private final TeacherAccountService teacherService;
    private final StudentAccountService studentAccountService;
    private final CourseService courseService;

    @Autowired
    public AccountInit(TeacherAccountService teacherService, StudentAccountService studentAccountService, CourseService courseService){
        this.teacherService = teacherService;
        this.studentAccountService = studentAccountService;
        this.courseService = courseService;
    }

    public void generateTeacherAccount(String username, String password, String firstName, String lastName, Integer locationId) {
        TeacherAccount newTeacher = new TeacherAccountBuilder()
                .setUsername(username)
                .setPassword(password)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setLocation(locationId)
                .build();
        teacherService.persist(newTeacher);
    }

    public void generateStudentAccount(String username, String password, String firstName, String lastName) {
        StudentAccount newStudent = new StudentAccountBuilder()
                .setUsername(username)
                .setPassword(password)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setCredits(0)
                .setProgram("OI")
                .setSchedule(1)
                .build();
        studentAccountService.persist(newStudent);
    }

    public void generateCourse() {
        Course temp = new Course();
        temp.setName("Algebra");
        temp.setCapacity(90);
        temp.setCredits(6);
        temp.setClassIds(Arrays.asList(1, 2));
        temp.setStudents(studentAccountService.findAllStudents());
        temp.setTeachers(teacherService.findAllTeacherAccounts());
        courseService.persist(temp);
    }

}
