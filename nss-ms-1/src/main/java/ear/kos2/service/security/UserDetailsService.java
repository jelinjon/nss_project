package ear.kos2.service.security;


import ear.kos2.dao.AccountDao;
import ear.kos2.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final AccountDao userDao;

    @Autowired
    public UserDetailsService(AccountDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Account user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Account with username " + username + " not found.");
        }
        return new ear.kos2.security.model.UserDetails(user);
    }
}

