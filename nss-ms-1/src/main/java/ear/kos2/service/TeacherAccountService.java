package ear.kos2.service;

import ear.kos2.dao.AccountDao;
import ear.kos2.dao.CourseDao;
import ear.kos2.dao.TeacherAccountDao;
import ear.kos2.exception.AccountException;
import ear.kos2.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.Objects;

@Service
public class TeacherAccountService extends AccountService {

    private final TeacherAccount teacherAccount = new TeacherAccount();
    private final TeacherAccountDao teacherAccountDao;
    private final PasswordEncoder passwordEncoder;
    private final RestTemplate restTemplate;


    @Autowired
    public TeacherAccountService(AccountDao accountDao,
                                 CourseDao courseDao,
                                 TeacherAccountDao teacherAccountDao,
                                 PasswordEncoder passwordEncoder,
                                 RestTemplate restTemplate) {
        super(accountDao, courseDao);
        this.teacherAccountDao = teacherAccountDao;
        this.passwordEncoder = passwordEncoder;
        this.restTemplate = restTemplate;
    }

    @Transactional
    public List<Course> showMyCourses(){
        return teacherAccount.getCourses();
    }

    @Transactional
    public void createCourse(){
        Course course = new Course();
        course.setName("name");
        course.setCapacity(10);
        // ...
        courseDao.persist(course);
    }

    @Transactional
    public void editCourse(int id, String name, int credits, int capacity){
        Course course = courseDao.find(id);
        if(course == null){
            throw new AccountException("Course with that id does not exist");
        }

        if(name != null){
            course.setName(name);
        }
        else{
            throw new AccountException("Name of course cannot be null");
        }
        if(credits > 0){
            course.setCredits(credits);
        }
        else {
            throw new AccountException("Number of credits has to exceed 0");
        }
        if(capacity > 0){
            course.setCapacity(capacity);
        }
        else {
            throw new AccountException("Capacity has to exceed 0");
        }

        courseDao.update(course);
    }

    @Transactional
    public TeacherAccount find(Integer id) {
        return teacherAccountDao.find(id);
    }

    @Transactional
    public List<TeacherAccount> findAllTeacherAccounts() {
        return teacherAccountDao.getAllTeacherAccounts();
    }

    @Transactional
    public void update(TeacherAccount teacher) {
        teacherAccountDao.update(teacher);
    }

    @Transactional
    public void persist(TeacherAccount teacher) {
        Objects.requireNonNull(teacher);
        teacher.encodePassword(passwordEncoder);
        if (teacher.getRole() == null) {
            teacher.setRole(Role.TEACHER);
        }
        teacherAccountDao.persist(teacher);
    }
    @Transactional
    public ResponseEntity<LocationDTO> getTeacherLocation(Integer locationId) {
        String url = "http://localhost:8082/ClassService/location/" + locationId;
        try {
            return restTemplate.exchange(url, HttpMethod.GET, null, LocationDTO.class);
        } catch (RestClientException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
