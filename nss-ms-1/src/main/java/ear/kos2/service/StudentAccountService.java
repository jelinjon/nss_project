package ear.kos2.service;

import ear.kos2.dao.AccountDao;
import ear.kos2.dao.CourseDao;
import ear.kos2.dao.StudentAccountDao;
import ear.kos2.exception.AccountException;
import ear.kos2.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.Objects;


@Service
public class StudentAccountService extends AccountService {
    private final StudentAccountDao studentAccountDao;
    private final StudentAccount studentAccount = new StudentAccount();
    private final PasswordEncoder passwordEncoder;
    private final RestTemplate restTemplate;

    @Autowired
    public StudentAccountService(AccountDao accountDao,
                                 CourseDao courseDao,
                                 StudentAccountDao studentAccountDao,
                                 PasswordEncoder passwordEncoder,
                                 RestTemplate restTemplate) {
        super(accountDao, courseDao);
        this.studentAccountDao = studentAccountDao;
        this.passwordEncoder = passwordEncoder;
        this.restTemplate = restTemplate;
    }

    @Transactional
    public List<Course> showMyCourses(){
        return studentAccount.getCourses();
    }

    @Transactional
    @Cacheable(value = "students", key = "#id")
    public StudentAccount findStudent(int id){
        return studentAccountDao.findStudentForId(id);
    }

    @Transactional
    @Cacheable(value = "students", key = "#root.methodName")
    public List<StudentAccount> findAllStudents() {
        return studentAccountDao.getAllStudentAccounts();
    }

    @Transactional
    public void addCourse(int id){
        Course course = courseDao.find(id);
        if(course == null){
            throw new AccountException("Course with that id does not exist");
        }
        studentAccount.addToCourses(course);
        studentAccountDao.update(studentAccount);
    }

    @Transactional
    public void removeCourse(int id){
        Course course = courseDao.find(id);
        if(course == null){
            throw new AccountException("Course with that id does not exist");
        }
        studentAccount.removeFromCourses(course);
        studentAccountDao.update(studentAccount);
    }

    @Transactional
    public ResponseEntity<String> addToSchedule(Integer userId, Integer classId) {
        String url = "http://localhost:8082/ClassService/schedule/add_class/" + classId + "/student/" + userId;

        try {
            return restTemplate.exchange(url, HttpMethod.POST, null, String.class);
        } catch (RestClientException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Unexpected error during join process.");
        }
    }

    @Transactional
    public ScheduleDTO getScheduleById(int idAccount) {
        int scheduleId = studentAccountDao.find(idAccount).getSchedule();
        try {
            ResponseEntity<ScheduleDTO> response = restTemplate.exchange(
                    "http://localhost:8082/ClassService/schedule/user/{id}",
                    HttpMethod.GET,
                    null,
                    ScheduleDTO.class,
                    scheduleId
            );
            return response.getBody();
        } catch (RestClientException e) {
            return null;
        }
    }

    @Transactional
    public void persist(StudentAccount student) {
        Objects.requireNonNull(student);
        student.encodePassword(passwordEncoder);
        if (student.getRole() == null) {
            student.setRole(Role.STUDENT);
        }
        studentAccountDao.persist(student);
    }

    @Transactional
    public void update(StudentAccount student) {
        studentAccountDao.update(student);
    }

    @Transactional
    public void remove(StudentAccount student){
        studentAccountDao.remove(student);
    }
}
