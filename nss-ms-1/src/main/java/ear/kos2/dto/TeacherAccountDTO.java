package ear.kos2.dto;

import ear.kos2.model.AbstractEntity;
import ear.kos2.model.TeacherAccount;

import java.util.List;
import java.util.stream.Collectors;

public class TeacherAccountDTO {
    public Integer id;
    public String username;
    public String password;
    public String firstName;
    public String lastName;
    public Integer schedule;
    public String role;
    public String title;
    public Integer location;
    public String program;
    public Integer credits;
    public List<Integer> courses;

    public TeacherAccountDTO(TeacherAccount s) {
        this.id = s.getId();
        this.username = s.getUsername();
        this.password = s.getPassword();
        this.firstName = s.getFirstName();
        this.lastName = s.getLastName();
        this.schedule = s.getSchedule();
        this.role = s.getRole().toString();
        this.title = s.getTitle();
        this.location = s.getLocation();
        try {
            this.courses = s.getCourses().stream().map(AbstractEntity::getId).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
            this.courses = List.of();
        }
    }
}

