package ear.kos2.dto;

import ear.kos2.model.AbstractEntity;
import ear.kos2.model.StudentAccount;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class StudentAccountDTO {
    public Integer id;
    public String username;
    public String password;
    public String firstName;
    public String lastName;
    public Integer schedule;
    public String role;
    public String program;
    public Integer credits;
    public List<Integer> courses;

    public StudentAccountDTO(StudentAccount s) {
        this.id = s.getId();
        this.username = s.getUsername();
        this.password = s.getPassword();
        this.firstName = s.getFirstName();
        this.lastName = s.getLastName();
        this.schedule = s.getSchedule();
        this.role = s.getRole().toString();
        this.program = s.getProgram();
        this.credits = s.getCredits();

        if(s.getCourses() == null) {
            this.courses = Collections.emptyList();
        } else {
            this.courses = s.getCourses().stream().map(AbstractEntity::getId).collect(Collectors.toList());
        }
    }
}
