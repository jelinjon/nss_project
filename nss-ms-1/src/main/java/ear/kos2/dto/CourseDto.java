package ear.kos2.dto;

import ear.kos2.model.AbstractEntity;
import ear.kos2.model.Course;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CourseDto {

    private Integer id;
    private String name;
    private Integer credits;
    private Integer capacity;
    private List<Integer> teachers;
    private List<Integer> students;
    private List<Integer> classIds;

    public CourseDto(Course c) {
        this.id = c.getId();
        this.name = c.getName() ;
        this.credits = c.getCredits();
        this.capacity = c.getCapacity() ;
        if(c.getTeachers() == null) {
            this.teachers = Collections.emptyList();
        } else {
            this.teachers = c.getTeachers().stream().map(AbstractEntity::getId).collect(Collectors.toList());
        }
        if(c.getStudents() == null) {
            this.students = Collections.emptyList();
        } else {
            this.students = c.getStudents().stream().map(AbstractEntity::getId).collect(Collectors.toList());
        }
        if(c.getClassIds() == null) {
            this.classIds = Collections.emptyList();
        } else {
            this.classIds = c.getClassIds();
        }


    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public List<Integer> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Integer> teachers) {
        this.teachers = teachers;
    }

    public List<Integer> getStudents() {
        return students;
    }

    public void setStudents(List<Integer> students) {
        this.students = students;
    }

    public List<Integer> getClassIds() {
        return classIds;
    }

    public void setClassIds(List<Integer> classIds) {
        this.classIds = classIds;
    }

    @Override
    public String toString() {
        return "CourseDto{" +
                "id=" + id +
                ", name=" + name + '\'' +
                ", credits=" + credits +
                ", capacity=" + capacity +
                ", teacher ids=" + teachers +
                ", student ids=" + students +
                ", class ids=" + classIds +
                '}';
    }
}
