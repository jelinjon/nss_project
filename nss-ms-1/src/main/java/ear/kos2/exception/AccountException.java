package ear.kos2.exception;

public class AccountException extends RuntimeException {
    public AccountException(String message) {
        super(message);
    }
}
