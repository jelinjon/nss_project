package ear.kos2.repository;

import ear.kos2.model.Course;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends ElasticsearchRepository<Course, String> {

    // Find courses by name
    List<Course> findByName(String name);

    // Find courses by credits
    List<Course> findByCredits(Integer credits);

    // Find courses by capacity
    List<Course> findByCapacity(Integer capacity);

    // Find courses by teacher ID
    @Query("{\"nested\": {\"path\": \"teachers\", \"query\": {\"bool\": {\"must\": [{\"match\": {\"teachers.id\": \"?0\"}}]}}}}")
    List<Course> findByTeachers_Id(String teacherId);
}