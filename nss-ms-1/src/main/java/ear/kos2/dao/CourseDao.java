package ear.kos2.dao;

import ear.kos2.model.Course;
import ear.kos2.model.StudentAccount;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CourseDao extends BaseDao<Course>{
    protected CourseDao() {
        super(Course.class);
    }

    public List<Course> findAll(String name){
        List<Course>result = new ArrayList<>();
        for (Course c : this.findAll()) {
            if(c.getName().equals(name)){
                result.add(c);
            }
        }
        if(result.isEmpty()){
            throw new IllegalArgumentException("No courses with that name exist");
        }
        return result;
    }

    @Override
    public Course find(Integer id) {
        try {
            return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e WHERE e.id = :id", Course.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (RuntimeException e) {
            return null;
        }
    }
}
