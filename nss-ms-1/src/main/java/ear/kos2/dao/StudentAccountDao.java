package ear.kos2.dao;

import ear.kos2.model.StudentAccount;
import jakarta.persistence.PersistenceException;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class StudentAccountDao extends BaseDao<StudentAccount> {
    protected StudentAccountDao() {
        super(StudentAccount.class);
    }


    public StudentAccount findStudentForId(Integer id) {
        try {
            return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e WHERE e.id = :id", StudentAccount.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (RuntimeException e) {
            return null;
        }
    }

    public List<StudentAccount> getAllStudentAccounts() {
        try {
            return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e", StudentAccount.class).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
