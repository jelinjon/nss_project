package ear.kos2.dao;

import ear.kos2.model.StudentAccount;
import ear.kos2.model.TeacherAccount;
import jakarta.persistence.PersistenceException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TeacherAccountDao extends BaseDao<TeacherAccount>{
    protected TeacherAccountDao() {
        super(TeacherAccount.class);
    }

    public List<TeacherAccount> getAllTeacherAccounts() {
        try {
            return em.createQuery("SELECT t FROM " + type.getSimpleName() + " t", TeacherAccount.class).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
