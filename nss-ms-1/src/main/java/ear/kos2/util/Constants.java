package ear.kos2.util;

import ear.kos2.model.Role;

public final class Constants {

    /**
     * Default student role.
     */
    public static final Role DEFAULT_ROLE = Role.STUDENT;

    /**
     * Username login form parameter.
     */
    public static final String USERNAME_PARAM = "username";

    private Constants() {
        throw new AssertionError();
    }
}
