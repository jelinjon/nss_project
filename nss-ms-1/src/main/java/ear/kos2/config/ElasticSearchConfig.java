package ear.kos2.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;

@Configuration
public class ElasticSearchConfig extends ElasticsearchConfiguration {
    @Override
    public ClientConfiguration clientConfiguration() {
        return ClientConfiguration.builder()
                .connectedTo("localhost:9200")
                .usingSsl("f6c3482e8c52691d4f0e0e976f438f4f15e83bd8e2bf6cea6d710ec59483edc0") //add the generated sha-256 fingerprint
//                .withBasicAuth("elastic", "Tp0-*eCdH2DPeO-jcHwd") //add your username and password
                .withBasicAuth("elastic", "c+cZjlX9RbX2aR6+dYp3") //add your username and password
                .build();
    }
}
