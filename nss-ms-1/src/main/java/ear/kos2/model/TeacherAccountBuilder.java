package ear.kos2.model;

import java.util.List;

public class TeacherAccountBuilder {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Role role;
    private List<Integer> classes;
    private List<Course> courses;
    private String title;
    private Integer location;


    public TeacherAccountBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public TeacherAccountBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public TeacherAccountBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TeacherAccountBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TeacherAccountBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public TeacherAccountBuilder setRole(Role role) {
        this.role = role;
        return this;
    }

    public TeacherAccountBuilder setClasses(List<Integer> classes) {
        this.classes = classes;
        return this;
    }

    public TeacherAccountBuilder setCourses(List<Course> courses) {
        this.courses = courses;
        return this;
    }

    public TeacherAccountBuilder setLocation(Integer location) {
        this.location = location;
        return this;
    }


    public TeacherAccount build() {
        TeacherAccount temp = new TeacherAccount();
        temp.setUsername(this.username);
        temp.setPassword(this.password);
        temp.setFirstName(this.firstName);
        temp.setLastName(this.lastName);
        temp.setTitle(this.title);
        temp.setRole(Role.TEACHER);
        temp.setClasses(this.classes);
        temp.setCourses(this.courses);
        temp.setLocation(this.location);
        return temp;
    }
}
