package ear.kos2.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "teacher")
@PrimaryKeyJoinColumn(name = "id_account")
public class TeacherAccount extends Account {
    @Basic
    @Column(name = "title", length = 20)
    private String title;

    @Basic
    @Column(name = "locationId", length = 20, nullable = true)
    private Integer locationId;

    @ManyToMany
    @JoinTable(name = "teacher_course", joinColumns = @JoinColumn(name = "id_teacher"), inverseJoinColumns = @JoinColumn(name = "id_course"))
    private List<Course> courses;

    @Basic
    @Column(name = "class_ids", nullable = true)
    private List<Integer> classIds;

    public List<Course> getCourses() {
        return courses;
    }

    public TeacherAccount(){
    }

    @Override
    public String toString(){
        return "Student{ " + "name=" + getFirstName() + " " + getLastName() + ", title=" + title + ", location=" + locationId
                + ", courses=" + courses + "}";
    }

    public void setLocation(Integer location) {
    this.locationId = location;
}
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLocation() {
    return locationId;
}

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Integer> getClasses() {
    return classIds;
}

    public void setClasses(List<Integer> classes) {
        this.classIds = classes;
    }
}
