package ear.kos2.model;

import ear.kos2.exception.GeneralException;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "student")
@PrimaryKeyJoinColumn(name = "id_account")
public class StudentAccount extends Account {
    @Basic
    @Column(name = "program", nullable = false, length = 20)
    private String program;
    @Basic
    @Column(name = "credits", nullable = false)
    private Integer credits;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "student_course", joinColumns = @JoinColumn(name = "id_student"), inverseJoinColumns = @JoinColumn(name = "id_course"))
    private List<Course> courses;

    public void addToCourses(Course course){
        Objects.requireNonNull(course);

        if(courses == null){
            courses = new ArrayList<>();
            courses.add(course);
            return;
        }

        if(!courses.contains(course)){
            courses.add(course);
        }
    }

    public void removeFromCourses(Course course){
        Objects.requireNonNull(course);

        if(courses.contains(course)){
            courses.remove(course);
        }
        else {
            throw new GeneralException();
        }
    }

    @Override
    public String toString(){
        return "Student{ " + "id=" + getId() + ", name=" + getFirstName() + " " + getLastName() + ", program=" + program + ", credits=" + credits + "}";
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
