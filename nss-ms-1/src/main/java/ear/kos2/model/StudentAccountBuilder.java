package ear.kos2.model;

import java.util.List;

public class StudentAccountBuilder {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Role role;
    private List<Course> courses;
    private String program;
    private Integer credits;
    private Integer schedule;


    public StudentAccountBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public StudentAccountBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public StudentAccountBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public StudentAccountBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public StudentAccountBuilder setCourses(List<Course> courses) {
        this.courses = courses;
        return this;
    }

    public StudentAccountBuilder setCredits(Integer credits) {
        this.credits = credits;
        return this;
    }
    public StudentAccountBuilder setSchedule(Integer schedule) {
        this.schedule = schedule;
        return this;
    }

    public StudentAccountBuilder setProgram(String program) {
        this.program = program;
        return this;
    }


    public StudentAccount build() {
        StudentAccount temp = new StudentAccount();
        temp.setUsername(this.username);
        temp.setPassword(this.password);
        temp.setFirstName(this.firstName);
        temp.setLastName(this.lastName);
        temp.setRole(Role.STUDENT);
        temp.setCourses(this.courses);
        temp.setCredits(this.credits);
        temp.setProgram(this.program);
        temp.setSchedule(this.schedule);
        return temp;
    }
}

