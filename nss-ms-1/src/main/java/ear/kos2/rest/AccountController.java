package ear.kos2.rest;

import ear.kos2.util.RestUtils;
import ear.kos2.security.model.UserDetails;
import ear.kos2.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/logout")
    public ResponseEntity<Void> logout() {
        try {
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
            return new ResponseEntity<>(headers, HttpStatus.I_AM_A_TEAPOT);
        } catch (Exception e) {
            LOG.debug("Login unsuccessful");
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/home");
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getCurrent(Authentication auth) {
        assert auth.getPrincipal() instanceof UserDetails;
        return ((UserDetails) auth.getPrincipal()).getUser().getFirstName() +
                " " + ((UserDetails) auth.getPrincipal()).getUser().getLastName() +
                " " + ((UserDetails) auth.getPrincipal()).getUser().getRole();
    }
}
