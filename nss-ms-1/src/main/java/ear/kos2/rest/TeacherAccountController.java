package ear.kos2.rest;

import ear.kos2.dto.CourseDto;
import ear.kos2.dto.TeacherAccountDTO;
import ear.kos2.exception.NotFoundException;
import ear.kos2.model.Course;
import ear.kos2.model.LocationDTO;
import ear.kos2.model.TeacherAccount;
import ear.kos2.util.RestUtils;
import ear.kos2.service.TeacherAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/teacher")
public class TeacherAccountController {
    private static final Logger LOG = LoggerFactory.getLogger(TeacherAccountController.class);
    private final TeacherAccountService teacherAccountService;

    @Autowired
    public TeacherAccountController(TeacherAccountService teacherAccountService) {
        this.teacherAccountService = teacherAccountService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> login(@RequestBody TeacherAccount user) {
        try {
            teacherAccountService.login(user.getUsername(), user.getPassword());
            LOG.debug("User {} successfully logged in.", user);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } catch (Exception e) {
            LOG.debug("Login unsuccessful");
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/home");
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/classes")
    public List<CourseDto> getCourses() {
        return teacherAccountService.showMyCourses().stream().map(this::convertToCourseDTO).collect(Collectors.toList());
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TeacherAccountDTO getTeacher(@PathVariable Integer id) {
        final TeacherAccount p = teacherAccountService.find(id);
        if (p == null) {
            throw NotFoundException.create("Teacher", id);
        }
        return convertToTeacherAccountDTO(p);
    }

    @GetMapping(value = "/{id}/location", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LocationDTO> getTeacherLocation(@PathVariable Integer id) {
        final TeacherAccount p = teacherAccountService.find(id);
        if (p == null) {
            throw NotFoundException.create("Teacher", id);
        }
        return teacherAccountService.getTeacherLocation(p.getLocation());
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TeacherAccountDTO> getTeachers() {
        return teacherAccountService.findAllTeacherAccounts().stream().map(this::convertToTeacherAccountDTO).collect(Collectors.toList());
    }

    public CourseDto convertToCourseDTO(Course course) {
        return new CourseDto(course);
    }
    public TeacherAccountDTO convertToTeacherAccountDTO(TeacherAccount teacher) {
        return new TeacherAccountDTO(teacher);
    }
}


