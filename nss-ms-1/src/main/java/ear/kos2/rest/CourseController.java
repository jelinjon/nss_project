package ear.kos2.rest;

import ear.kos2.dto.CourseDto;
import ear.kos2.dto.StudentAccountDTO;
import ear.kos2.exception.ValidationException;
import ear.kos2.model.Course;
import ear.kos2.model.StudentAccount;
import ear.kos2.repository.CourseRepository;
import ear.kos2.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/courses")
public class CourseController {
    private static final Logger LOG = LoggerFactory.getLogger(CourseController.class);

    private final CourseService courseService;
    private final CourseRepository courseRepository;

    @Autowired
    public CourseController(CourseService courseService, CourseRepository courseRepository) {
        this.courseService = courseService;
        this.courseRepository = courseRepository;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDto> getCourses() {
        return courseService
                .findAll()
                .stream()
                .map(this::convertCourseToDTO)
                .collect(Collectors.toList());
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CourseDto> createCourse(@RequestBody Course course) {
        courseService.persist(course); //normal DB persist
        courseRepository.save(course); //elasticsearch repository persist

        LOG.info("Created course {}.", course);
        return ResponseEntity.status(HttpStatus.CREATED).body(convertCourseToDTO(course));
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CourseDto> getCourse(@PathVariable Integer id) {
        // non elastic + elastic implementation
        try {
            final Optional<Course> p = courseRepository.findById(String.valueOf(id));
            if(p.isPresent()) {
//                return ResponseEntity.status(HttpStatus.FOUND).body(convertCourseToDTO(p.get()));
                return ResponseEntity.status(HttpStatus.FOUND).body(convertCourseToDTO(p.get()));
            } else {
                final Course c = courseService.find(id);
                if (c == null) {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
                }
//                return ResponseEntity.status(HttpStatus.FOUND).body(convertCourseToDTO(c));
                return ResponseEntity.status(HttpStatus.FOUND).body(convertCourseToDTO(c));
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    // elastic search methods
    @GetMapping(value = "/search/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDto> searchByName(@PathVariable String name) {
        return courseRepository.findByName(name).stream().map(this::convertCourseToDTO).collect(Collectors.toList());
    }
    @GetMapping(value = "/search/credits/{credits}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDto> searchByCredits(@PathVariable Integer credits) {
        return courseRepository.findByCredits(credits).stream().map(this::convertCourseToDTO).collect(Collectors.toList());
    }
    @GetMapping(value = "/search/capacity/{capacity}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDto> searchByCapacity(@PathVariable Integer capacity) {
        return courseRepository.findByCapacity(capacity).stream().map(this::convertCourseToDTO).collect(Collectors.toList());
    }
    @GetMapping("/search/teacher/{teacherId}")
    public List<CourseDto> getByTeacherId(@PathVariable String teacherId) {
        return courseRepository.findByTeachers_Id(teacherId).stream().map(this::convertCourseToDTO).collect(Collectors.toList());
    }


    @PostMapping(value = "/create_test_course", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> insertCourseElastic() {
        try {
            Course temp = new Course();
            temp.setName("new elastic course");
            temp.setCredits(4);
            temp.setCapacity(40);
            courseService.persist(temp);
            courseRepository.save(temp);

            return ResponseEntity.status(HttpStatus.OK).body("Success");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Unexpected failure");
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCourse(@PathVariable Integer id, @RequestBody Course course) {
        final Course original = courseService.find(id);
        if (!original.getId().equals(course.getId())) {
            throw new ValidationException("Course identifier in the data does not match the one in the request URL.");
        }
        courseService.update(course);
        LOG.debug("Updated course {}.", course);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCourse(@PathVariable Integer id) {
        final Course toRemove = courseService.find(id);
        if (toRemove == null) {
            return;
        }
        courseService.remove(toRemove);
        LOG.debug("Removed course {}.", toRemove);
    }

    @GetMapping(value = "/{id}/classes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Integer> getClassesByCourseId(@PathVariable Integer id) {
        final Course c = courseService.find(id);
        if (c == null) {
            return null;
        }
        return c.getClassIds();
    }

    @GetMapping(value = "/{id}/students", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<StudentAccountDTO>> getStudentsByCourseId(@PathVariable Integer id) {
        final Course c = courseService.find(id);
        if (c == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        if(c.getStudents() != null){
            LOG.info(c.getStudents().toString());
        }
        List<StudentAccount> st = c.getStudents();
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(st
                        .stream()
                        .map(this::convertStudentToDTO)
                        .collect(Collectors.toList())
                );
    }

    public CourseDto convertCourseToDTO(Course c) {
        return new CourseDto(c);
    }
    public StudentAccountDTO convertStudentToDTO(StudentAccount s) {
        return new StudentAccountDTO(s);
    }
}