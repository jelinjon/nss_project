package ear.kos2.rest;

import ear.kos2.dto.StudentAccountDTO;
import ear.kos2.model.Course;
import ear.kos2.model.ScheduleDTO;
import ear.kos2.model.StudentAccount;
import ear.kos2.service.CourseService;
import ear.kos2.service.StudentAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/student")
public class StudentAccountController {
    private static final Logger LOG = LoggerFactory.getLogger(StudentAccountController.class);
    private final StudentAccountService studentAccountService;
    private final CourseService courseService;
@Autowired
public StudentAccountController(StudentAccountService studentAccountService,
                                CourseService courseService)
{
    this.studentAccountService = studentAccountService;
    this.courseService = courseService;
}
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StudentAccountDTO> getStudents() {
        return studentAccountService.findAllStudents().stream().map(this::convertStudentToDTO).collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public StudentAccountDTO getStudent(@PathVariable Integer id) {
        final StudentAccount p = studentAccountService.findStudent(id);
        if (p == null) {
            LOG.info("Student with id " + id + " was not found.");
            return null;
        }
        return convertStudentToDTO(p);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path = "/create")
    public ResponseEntity<StudentAccountDTO> createStudent(@RequestBody StudentAccount student) {
        try {
            studentAccountService.persist(student);
            return ResponseEntity.status(HttpStatus.CREATED).body(convertStudentToDTO(student));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
    }

    @GetMapping(value = "/{id}/schedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public ScheduleDTO getScheduleByUserId(@PathVariable Integer id) {
        return studentAccountService.getScheduleById(id);
    }

    @GetMapping(value = "/{id}/join/course/{courseId}")
    public ResponseEntity<String> joinCourse(@PathVariable Integer id, @PathVariable Integer courseId) {
        Course course = null;
        StudentAccount user = null;
        try {
            course = courseService.find(courseId);
            user = studentAccountService.findStudent(id);

            if(course == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Course not found.");
            }
            if(user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found.");
            }
        } catch (Exception e1) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Course or user not found.");
        }
        try{
            if (!course.getStudents().contains(user)) {
                course.addToStudents(user);
            }
            if (!user.getCourses().contains(course)) {
                user.addToCourses(course);
            }

            // Update credits
            user.setCredits(user.getCredits() + course.getCredits());

            // Save changes
            courseService.update(course);
            studentAccountService.update(user);

            LOG.info(course.toString());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("Participants found, course joined or left.");
        } catch (Exception e2) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unexpected error ocurred.");
        }
    }

    @PostMapping(value = "/{id}/join/class/{class_id}")
    public ResponseEntity<String> joinClass(@PathVariable Integer id, @PathVariable Integer class_id) {
        try{
            return studentAccountService.addToSchedule(id, class_id);
        } catch (Exception e2) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Unexpected error during join process for student id " + id + ", and class id " + class_id);
        }
    }

    public StudentAccountDTO convertStudentToDTO(StudentAccount s) {
        return new StudentAccountDTO(s);
    }
}