INSERT INTO course (name, credits, capacity) VALUES ('Physics 1', 10, 100);
--INSERT INTO course (name, credits, capacity) VALUES ('course 2', 20, 200);
--INSERT INTO course (name, credits, capacity) VALUES ('course 3', 30, 220);

--INSERT INTO account (username, password, first_name, last_name) VALUES ('sample username', 'sample password', 'John', 'Deer');

-- Insert into the account table
--INSERT INTO account (username, password, first_name, last_name, schedule)
--VALUES ('john_doe', 'password123', 'John', 'Doe', 3);

-- Use a temporary table to store the last inserted ID
--CREATE TABLE temp_id AS SELECT MAX(id) AS id FROM account;

-- Insert into the student table using the last inserted id from the account table
--INSERT INTO student (id_account, program, credits)
--SELECT id, 'Computer Science', 30 FROM temp_id;

-- Clean up the temporary table
--DROP TABLE temp_id;